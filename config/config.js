module.exports = {
  development: {
    username: 'ohb',
    password: 'ohb',
    database: 'one_hour_beats',
    host: 'onehourbeats.com',
    dialect: 'sqlite',
    storage: './database.sqlite3',
  },
  test: {
    username: 'ohb',
    password: 'ohb',
    database: 'one_hour_beats_test',
    host: '127.0.0.1',
    dialect: 'postgres',
  },
  production: {
    username: 'ohb',
    password: 'ohb',
    database: 'one_hour_beats',
    host: '127.0.0.1',
    dialect: 'postgres',
  },
}
