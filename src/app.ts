// @ts-nocheck

import * as express from 'express'
import { Sequelize } from 'sequelize'
import * as socket from 'socket.io'
import { createDatabase, User } from '../models'
import { User } from '../models/user'
import { Entry } from '../models/entry'
import { Jam } from '../models/jam'
import { VoteToken } from '../models/votetoken'

type Env = {
  PORT: string
  NODE_ENV: string
}

type Params = {
  env: any
}

export interface App extends Params {
  server: express.Express
  io: socket.Server
  db: Sequelize
  models: {
    User: User
    Entry: Entry
    Jam: Jam
    VoteToken: VoteToken
  }
}

export class App {
  constructor({ env }: Params) {
    this.env = env
    this.server = express()
    this.server.use(express.static(__dirname + '/../public'))
    this.server.use(express.urlencoded({ extended: true }))
    this.server.use(express.json())

    this.db = new Sequelize(env.DB_NAME, env.DB_USER, env.DB_PASS, {
      dialect: env.DB_TYPE,
      storage: './database.sqlite3',
    })

    createDatabase(this.db)

    this.testDB()

    this.server.listen(this.env.PORT)
    this.io = socket()

    this.server.get('/', async (req, res) => {
      try {
        const users = await this.db.models.User.findAll()
        console.log('users', users)
        res.send(users)
      } catch (err) {
        console.error(err)
        res.send({ ...err, message: 'EVERYTHING IS FUCKED' })
      }
    })

    console.log(`listening on ${this.env.PORT}`)
  }

  testDB = async () => {
    try {
      await this.db.sync({ force: true })
      const user = {
        id: 'eli8vh',
        username: 'eli8vh',
        name: 'Elijahhhhhh',
        password: 'blahbalh',
        email: 'blahboy@gmail.com',
      }
      console.log('CREATING USER', user)
      await this.db.models.User.create(user)
      await this.db.authenticate()
      console.log(await this.db.models.User.findAll())
      console.log('Database connected')
    } catch (error) {
      console.error(error)
    }
  }
}
