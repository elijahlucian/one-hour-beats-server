import * as dotenv from 'dotenv'

import { App } from './app'

const env = dotenv.config().parsed

const app = new App({ env })
