import { Sequelize, DataTypes, Association, Model } from 'sequelize'

export class User extends Model {
  public username!: string
  public name!: string
  public email!: string
  public password!: string
  public thumbs: number
  public wins: number
}

export class Jam extends Model {
  name: string
  description: string
  timeLimit: number
  userId: string
  startedAt: Date
}

export class Entry extends Model {
  public id!: string
  public link!: string
  public title!: string
  public userId!: string
  public jamId!: string

  public readonly createdAt!: Date
  public readonly updatedAt!: Date

  public readonly user: User
  public readonly jam: Jam

  public static associations: {
    user: Association<Entry, User>
    jam: Association<Entry, Jam>
  }
}

export class VoteToken extends Model {
  userId: string
  jamId: string
  entryId: string

  public readonly user: User
  public readonly jam: Jam
  public readonly entry?: Entry

  public static associations: {
    user: Association<VoteToken, User>
    jam: Association<VoteToken, Jam>
    entry?: Association<VoteToken, Entry>
  }
}

export const createDatabase = async (sequelize: Sequelize) => {
  User.init(
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      username: {
        type: new DataTypes.STRING(128),
      },
      name: {
        type: new DataTypes.STRING(128),
      },
      email: {
        type: new DataTypes.STRING(128),
      },
      password: {
        type: new DataTypes.STRING(128),
      },
      thumbs: {
        type: DataTypes.INTEGER.UNSIGNED,
        defaultValue: 0,
      },
      wins: {
        type: DataTypes.INTEGER.UNSIGNED,
        defaultValue: 0,
      },
    },
    {
      tableName: 'users',
      sequelize,
    }
  )

  Jam.init(
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      name: {
        type: new DataTypes.STRING(128),
      },
      description: {
        type: new DataTypes.STRING(128),
      },
      timeLimit: {
        type: DataTypes.INTEGER.UNSIGNED,
      },
      userId: {
        type: DataTypes.STRING(),
        primaryKey: true,
      },
      startedAt: {
        type: DataTypes.DATE,
      },
    },
    {
      tableName: 'jams',
      sequelize,
    }
  )

  Entry.init(
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      link: {
        type: new DataTypes.STRING(128),
      },
      title: {
        type: new DataTypes.STRING(128),
      },
      userId: {
        type: new DataTypes.STRING(128),
        primaryKey: true,
      },
      jamId: {
        type: new DataTypes.STRING(128),
        primaryKey: true,
      },
      startedAt: {
        type: new DataTypes.DATE(),
      },
    },
    {
      tableName: 'entries',
      sequelize,
    }
  )

  VoteToken.init(
    {
      userId: {
        type: DataTypes.STRING(),
        primaryKey: true,
      },
      jamId: {
        type: DataTypes.STRING(),
        primaryKey: true,
      },
      entryId: {
        type: DataTypes.STRING(),
        allowNull: true,
      },
    },
    {
      timestamps: false,
      tableName: 'vote_tokens',
      sequelize,
    }
  )

  return { User, Entry, Jam, VoteToken }
}
